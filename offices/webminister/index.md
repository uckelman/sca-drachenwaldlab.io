---
title: Office of the Web Minister
subtitle: Who keeps this running?
---

The web minister, supported by several web artificers keeps the website running.

## Reporting
Mostly, Yda would like to hear from web ministers and would like to know if there's anything you're running into that kingdom web artificers can help with. Beyond that she appreciates getting an update on major things planned or that happened.  You can contact her at <a href="mailto:webminister@drachenwald.sca.org">webminister@drachenwald.sca.org</a>, and she'll be pleased to hear from you.

## Updating information on the website
* [Update officer information](https://forms.gle/Xm7bCu7nkq5uMU5Z6)
* [Adding a chartered group to the website]({{ site.baseurl}}{% link offices/webminister/content-policy.md %}) 

## Contact Information
<p>You can email the web minister at webminister@drachenwald.sca.org or if you notice an issue fill out an issue report at <a href="https://gitlab.com/sca-drachenwald/sca-drachenwald.gitlab.io/-/issues/new"> our gitlab issue tracker</a></p>

### Web minister
Yda van Boulogne

### Web artificers
 
* Sela de la Rosa 
* Genevieve La Flechiere
* Aodh 
* Lydia von Are 

