---
title: "Dragon's Tale: kingdom newsletter"
toc: true
toc_label: Contents
excerpt: Sharing the best news 'round the kingdom
---

# The Dragon's Tale
The _Dragon's Tale_ is the official kingdom newsletter of the Kingdom of Drachenwald and is published monthly, available online and by post to subscribing members.
The newsletter is the official source for all Kingdom business, such as

* changes to law
* Royal proclamations
* communications from the Crown and Kingdom Officers 
* event announcements 

It also contains the official contact list for Kingdom Officers and local seneschals.
In addition to the above, the newsletter also publishes court reports, the results of heraldic submissions and articles on matters of interest to SCA folk.

## Subscriptions

Subscriptions are only available to SCA Inc members and SCA affiliate members.

SCA Inc membership (and access to the newsletter): [SCA Member Services](https://www.sca.org/member-services/)

For those receiving paper copies, these are typically posted around the 20th of the month before the month of issue. If you have not received your newsletter by the 1st of the month, contact the Chronicler.
Paper copies going to APO addresses may take longer.

## Change of Address
Your subscription to _Dragon's Tale_ uses your membership postal or email address. So to change your address, you need to update your membership information, either via [SCA Inc member services](https://www.sca.org/member-services/) or through your local affiliate.  **Do not send address changes to the Chronicler.**

# Submissions to the Dragon's Tale

Contributions of articles, poems, quizzes, crosswords, artwork, photographs, etc for publication in the Dragon's Tale are always welcome.

If you have an idea for a contribution, please contact the Chronicler to see if it would be appropriate.

See [Guidelines for Contributors]({{ site.baseurl }}{% link offices/chronicler/guidelines-contributors.md %})
 
Contributions of original material require a permission form:
* <a href="{{ site.baseurl }}{% link offices/chronicler/files/permission.pdf %}">Permission Form (PDF)</a> - print, complete and post, or scan and email to Chronicler
* <a href="{{ site.baseurl }}{% link offices/chronicler/email-permission-form.md %}">Permission Form (email)</a> - copy to email, fill in blanks and email to Chronicler

## Advertising in the Dragon's Tale
You can publish advertising for events, products and services of interest to SCA members in the _Dragon's Tale_, subject to space being available.

Send advertising copy in the same formats as other contributions plus PDF. Payment required in advance: 

Advert size | Single issue | 3 or more issues
---|---|---
Full Page |£30 |£25 (per issue)
Half Page £15| £12 (per issue) 
Quarter Page| £10 |£8 (per issue)

Classified ads are charged at £2 per 100 words
